# ci-tools-pipeline-docker

This module contains abstract gitlab ci function for docker

## ci-docker

Since 1.0.0

Abstract ci function to push docker image on gitlab ci registry

### Usage

- extend .docker_build on any stage you want to push image
- set IMAGE_NAME, REGISTRY_URL, VERSION and optional DOCKER_BUILD_ARGS variables

```
include:
  - project: '$CI_PROJECT_ROOT_NAMESPACE/ci-tools/pipeline/ci-tools-pipeline-docker'
    ref: 1.0.0
    file: '/ci-docker.yml'

stages:
  - build

variables:
  IMAGE_NAME: $CI_PROJECT_NAME
  REGISTRY_URL: $CI_REGISTRY/$CI_PROJECT_NAMESPACE
  DOCKER_BUILD_ARGS: --build-arg CI_K8S_CONFIG="$CI_K8S_CONFIG"

build:
  except:
    - tags
  variables:
    VERSION: $CI_COMMIT_REF_SLUG
  extends: .build

build:tag:
  only:
    - tags
  variables:
    VERSION: $CI_COMMIT_TAG
  extends: .build

.build:
  image: $CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX/docker:latest
  stage: build
  extends: .docker_build
```
